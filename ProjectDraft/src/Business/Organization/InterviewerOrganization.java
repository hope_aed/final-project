package Business.Organization;

import Business.Role.InterviewerRole;
import Business.Role.LabRole;
import Business.Role.Role;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author don
 */
public class InterviewerOrganization extends Organization{
    


    public InterviewerOrganization() {
         super(Organization.Type.Interviewer.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new InterviewerRole());
        return roles;
}
}