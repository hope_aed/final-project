/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.LabRole;
import Business.Role.IdentificationOfficerRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author don
 */
public class IdentificationOrganisation extends Organization{
    public IdentificationOrganisation() {
        super(Organization.Type.Identification.getValue());
    }
    

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new IdentificationOfficerRole());
        return roles;
    
}
}
