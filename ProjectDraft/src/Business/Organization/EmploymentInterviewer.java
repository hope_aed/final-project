/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.DoctorRole;
import Business.Role.EmploymentInterviewerRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author don
 */
public class EmploymentInterviewer extends Organization{

    public EmploymentInterviewer() {
        super(Organization.Type.EmploymentInterviewer.getValue());
    }
    
     @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new EmploymentInterviewerRole());
        return roles;
    }
    
}
