/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.LabRole;
import Business.Role.MarketOrientedSkillProgramCordinatorRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author don
 */
public class MarketOrientedSkillOragnization extends Organization{

    public MarketOrientedSkillOragnization() {
        super(Organization.Type.MarketOrientedSkill.getValue());
    }
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new MarketOrientedSkillProgramCordinatorRole());
        return roles;
    }
}
