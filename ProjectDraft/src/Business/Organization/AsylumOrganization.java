/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.AsylumAllocatorRole;
import Business.Role.LabRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author don
 */
public class AsylumOrganization extends Organization{

    public AsylumOrganization() {
        super(Organization.Type.Asylum.getValue());
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new  AsylumAllocatorRole());
        return roles;
    
}
}
