/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.LabRole;
import Business.Role.HighSchoolAdmissionCordinatorRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author don
 */
public class HighSchoolOrganization extends Organization{

    public HighSchoolOrganization() {
        super(Organization.Type.HighSchool.getValue());
    }
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new HighSchoolAdmissionCordinatorRole());
        return roles;
    }
    
}
