/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.LabRole;
import Business.Role.Role;
import Business.Role.WageEmploymentCordinatorRole;
import java.util.ArrayList;

/**
 *
 * @author don
 */
public class WageEmploymentOragnization extends Organization{

    public WageEmploymentOragnization() {
        super(Organization.Type.WageEmployment.getValue());
    }
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new WageEmploymentCordinatorRole());
        return roles;
    }
    
}
