/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Refugee;

import java.util.Date;

/**
 *
 * @author don
 */
public class Refugee {
    
    private String firstName;
    private String lastName;
    private String gender;
    private String bloodGroup;
    private String   refugee_dob;
    private String homeCountry;
    private String qualification;
    private String workExperience;
    private String reasonForDisplacement;
    private String uniqueId;

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public Refugee() {
        
    }

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }
    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getRefugee_dob() {
        return refugee_dob;
    }
    public void setRefugee_dob(String refugee_dob) {
        this.refugee_dob = refugee_dob;
    }

    public String getHomeCountry() {
        return homeCountry;
    }
    public void setHomeCountry(String homeCountry) {
        this.homeCountry = homeCountry;
    }

    public String getQualification() {
        return qualification;
    }
    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getWorkExperience() {
        return workExperience;
    }
    public void setWorkExperience(String workExperience) {
        this.workExperience = workExperience;
    }

    public String getReasonForDisplacement() {
        return reasonForDisplacement;
    }
    public void setReasonForDisplacement(String reasonForDisplacement) {
        this.reasonForDisplacement = reasonForDisplacement;
    }
    
    
    @Override
    public String toString(){
        return uniqueId;

    }
    
}



