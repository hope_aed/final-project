/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.Organization;
import Business.Refugee.DetailsDirectory;
import Business.Refugee.RefugeeDirectory;
import Business.UserAccount.UserAccount;
import javax.swing.JPanel;
import userinterface.EduInterviewer.InterviewerJPanel;
import userinterface.EmploymentInterviewerRole.EmpInterviewerJPanel;
import userinterface.HealthDocRole.docWorkAreaJPanel;

/**
 *
 * @author don
 */
public class EmploymentInterviewerRole extends Role{
 
    @Override

    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, EcoSystem business,RefugeeDirectory refugeeDirectory, Network network,DetailsDirectory detailsdir) {
        return new EmpInterviewerJPanel(userProcessContainer,account,refugeeDirectory,organization,enterprise,business, network,detailsdir);
    }
    
    
    
}
