/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.Network.Network;
import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Organization.Organization;
import Business.Refugee.DetailsDirectory;
import Business.Refugee.RefugeeDirectory;
import Business.UserAccount.UserAccount;
import javax.swing.JPanel;

/**
 *
 * @author raunak
 */
public abstract class Role {
    
    public enum RoleType{
        Admin("Admin"),
        Doctor("Doctor"),
        LabAssistant("Lab Assistant"),
        Identification("Identification Officer"),
        Asylum("Asylum Organisation"),
        School("School Organisation"), 
        HighSchool("High School Organisation"),
        MarketOrientedSkill("Market Oriented Skill"), 
        WageEmployment("Wage Employment"),
        MicroSmallEmployment("Micro Small Employment"),
        SkilledEmployment("Skill Employment"),
        Interviewer("Edu Interviewer"),
        EmploymentInterviewer("Emp Interviewer");        
        private String value;
        private RoleType(String value){
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }
    }
    
    public abstract JPanel createWorkArea(JPanel userProcessContainer, 
            UserAccount account, 
            Organization organization, 
            Enterprise enterprise, 
            EcoSystem business,RefugeeDirectory refugeeDirectory, 
            Network network,DetailsDirectory detailsdir);

    @Override
    public String toString() {
        return this.getClass().getName();
    }
    
    
}