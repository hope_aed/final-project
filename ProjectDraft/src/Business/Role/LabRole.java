/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.Network.Network;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.LabOrganization;
import Business.Organization.Organization;
import Business.Refugee.DetailsDirectory;
import Business.Refugee.RefugeeDirectory;
import Business.UserAccount.UserAccount;
import userinterface.HealthlabRole.LabWorkAreaJPanel;
import javax.swing.JPanel;

/**
 *
 * @author raunak
 */
public class LabRole extends Role{

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, EcoSystem business,RefugeeDirectory refugeeDirectory, Network network,DetailsDirectory detailsdir) {
        return new LabWorkAreaJPanel(userProcessContainer, account, (LabOrganization)organization, enterprise, refugeeDirectory, network,detailsdir,business);
    }
}

