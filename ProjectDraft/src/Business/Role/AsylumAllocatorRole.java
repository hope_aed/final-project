/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.Network.Network;
import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Organization.LabOrganization;
import Business.Organization.Organization;
import Business.Refugee.DetailsDirectory;
import Business.Refugee.RefugeeDirectory;
import Business.UserAccount.UserAccount;
import javax.swing.JPanel;
import userinterface.HealthlabRole.LabWorkAreaJPanel;
import userinterface.NgoAsylumRole.AsylumAllocatorWokrAreaJPanel;

/**
 *
 * @author don
 */
public class AsylumAllocatorRole extends Role{
     @Override

    public JPanel createWorkArea(JPanel userProcessContainer,UserAccount account, Organization organization, Enterprise enterprise, EcoSystem business,RefugeeDirectory refugeeDirectory, Network network,DetailsDirectory detailsdir) {
        return new AsylumAllocatorWokrAreaJPanel(userProcessContainer ,account, organization, business, refugeeDirectory,enterprise, network,detailsdir);

    }
    
}
