/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.EduInterviewer;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.AsylumOrganization;
import Business.Organization.DoctorOrganization;
import Business.Organization.EmploymentInterviewer;
import Business.Organization.HighSchoolOrganization;
import Business.Organization.InterviewerOrganization;
import Business.Organization.MarketOrientedSkillOragnization;
import Business.Organization.Organization;
import static Business.Organization.Organization.Type.EmploymentInterviewer;
import Business.Organization.SchoolOrganization;
import Business.Organization.WageEmploymentOragnization;
import Business.Refugee.Details;
import Business.Refugee.DetailsDirectory;
import Business.Refugee.RefugeeDirectory;
import static Business.Role.Role.RoleType.EmploymentInterviewer;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.EducationWorkRequest;
import Business.WorkQueue.LabTestWorkRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author don
 */
public class InterviewerJPanel extends javax.swing.JPanel {
    
     private JPanel userProcessContainer;
    private EcoSystem business;
    private UserAccount account;
    //private Organization Organization;
    private Network network;
    private InterviewerOrganization intorganization;
    private Enterprise enterprise;
    private ArrayList<Details> tempList;
    private String fileName;
    private BufferedImage img;
    
    /**
     * Creates new form InterviewerJPanel
     */
    public InterviewerJPanel(JPanel userProcessContainer, UserAccount account, RefugeeDirectory refugeeDirectory,Organization organization, Enterprise enterprise, EcoSystem business, Network network,DetailsDirectory detailsdir) {
        initComponents();
        tempList = new ArrayList();
        this.userProcessContainer = userProcessContainer;
        this.account = account;
        this.business = business;
        this.intorganization = (InterviewerOrganization)organization;
        this.network = network;
        this.enterprise = enterprise;
        //this.organization=organization;
        this.fileName = "interviewer2.jpg";
        setBckImage(fileName,interviewerBckImageLbl);
        
        populateTable();
    }
    
     public void populateTable(){
          Organization org = null;
          
         for(Enterprise ent: network.getEnterpriseDirectory().getEnterpriseList()){
            for (Organization organization : ent.getOrganizationDirectory().getOrganizationList()){
                if (organization instanceof DoctorOrganization){
                    org = organization;
                    break;
                }
            }
        }
        if (org!=null){
            for(Details s : org.getDetailsdirectory().getDetailsDirectoryList()){
           if(s.getAsylum().equals("safe")){
                tempList.add(s);
        }
            }
        DefaultTableModel model = (DefaultTableModel)wRJTbl.getModel();
        
        model.setRowCount(0);
        
        for(Details request : tempList){
            Object[] row = new Object[3];
            row[0] = request;
            row[1] = request.getAsylum();
            row[2] =request.getSchool();
           
            
            model.addRow(row);
            
        }
        tempList.removeAll(tempList);
    }
     }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        wRJTbl = new javax.swing.JTable();
        refreshJButton = new javax.swing.JButton();
        admitToSchollBtn = new javax.swing.JButton();
        MOSBtn = new javax.swing.JButton();
        empBtn = new javax.swing.JButton();
        aHighSchoolBtn = new javax.swing.JButton();
        interviewerBckImageLbl = new javax.swing.JLabel();

        setLayout(null);

        wRJTbl.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        wRJTbl.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Unique ID", "Helath Status", "School Status"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(wRJTbl);

        add(jScrollPane1);
        jScrollPane1.setBounds(90, 60, 632, 170);

        refreshJButton.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        refreshJButton.setText("Refresh");
        refreshJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refreshJButtonActionPerformed(evt);
            }
        });
        add(refreshJButton);
        refreshJButton.setBounds(630, 10, 90, 30);

        admitToSchollBtn.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        admitToSchollBtn.setText("Admit to School");
        admitToSchollBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                admitToSchollBtnActionPerformed(evt);
            }
        });
        add(admitToSchollBtn);
        admitToSchollBtn.setBounds(290, 310, 300, 40);

        MOSBtn.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        MOSBtn.setText("Admit to Market Oriented Skill Program");
        MOSBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MOSBtnActionPerformed(evt);
            }
        });
        add(MOSBtn);
        MOSBtn.setBounds(290, 420, 300, 40);

        empBtn.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        empBtn.setText("Send to Employment Enterprise");
        empBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                empBtnActionPerformed(evt);
            }
        });
        add(empBtn);
        empBtn.setBounds(290, 480, 300, 30);

        aHighSchoolBtn.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        aHighSchoolBtn.setText("Admit to High School");
        aHighSchoolBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aHighSchoolBtnActionPerformed(evt);
            }
        });
        add(aHighSchoolBtn);
        aHighSchoolBtn.setBounds(290, 370, 300, 30);
        add(interviewerBckImageLbl);
        interviewerBckImageLbl.setBounds(0, 0, 1250, 700);
    }// </editor-fold>//GEN-END:initComponents
    
    private void setBckImage(String fileName, JLabel label){
        String pic = "C:\\Users\\Dell\\Desktop\\Final project aed\\New folder\\"+fileName;
        
        img = null;
        try {
            img = ImageIO.read(new File(pic));
            
            int scaledWidth = 1250;
            int scaledHeight = 750;

            Image dimg = img.getScaledInstance(scaledWidth, scaledHeight,Image.SCALE_SMOOTH);
            ImageIcon imageIcon = new ImageIcon(dimg);
            label.setIcon(imageIcon);
            } 
        catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Not able to read photo file");
            }
    }
    
    private void refreshJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refreshJButtonActionPerformed
        populateTable();
    }//GEN-LAST:event_refreshJButtonActionPerformed

    private void admitToSchollBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_admitToSchollBtnActionPerformed
        // TODO add your handling code here:
        int selectedRow = wRJTbl.getSelectedRow();
       // String message = docMsgTxtField.getText();

        if (selectedRow < 0){
            JOptionPane.showMessageDialog(null, "Please select Row !!");
           // String message = docMsgTxtField.getText();
        }
        else{
            
           Details request = (Details)wRJTbl.getValueAt(selectedRow, 0);
        request.setSchool("Admited to School");
       // request.setStatus("Completed");
        Organization org = null;
        for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()){
            if (organization instanceof SchoolOrganization){
                org = organization;
                break;
            }
        }
        if (org!=null){
            org.getDetailsdirectory().createDeatilsDir(request.getUniqueId(),"School");
        }
        
        populateTable();
            }
        
        
            
        
        
        
    }//GEN-LAST:event_admitToSchollBtnActionPerformed

    private void aHighSchoolBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aHighSchoolBtnActionPerformed
        // TODO add your handling code here:
        int selectedRow = wRJTbl.getSelectedRow();
       // String message = docMsgTxtField.getText();

        if (selectedRow < 0){
            JOptionPane.showMessageDialog(null, "Please select Row !!");
           // String message = docMsgTxtField.getText();
        }
        else{
            
           //WorkRequest request = (WorkRequest)wRJTbl.getValueAt(selectedRow, 0);
            Details request = (Details)wRJTbl.getValueAt(selectedRow, 0);
           JOptionPane.showMessageDialog(null, "Congrats you are Admited to High School !!");
          request.setSchool("Admited to High School");
        //request.setStatus("Completed");
       
        Organization org = null;
        for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()){
            if (organization instanceof HighSchoolOrganization){
                org = organization;
                break;
            }
        }
        if (org!=null){
            org.getDetailsdirectory().createDeatilsDir(request.getUniqueId(),"HighSchool");
        }
        
        populateTable();
            }
    }//GEN-LAST:event_aHighSchoolBtnActionPerformed

    private void MOSBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MOSBtnActionPerformed
        // TODO add your handling code here:
        int selectedRow = wRJTbl.getSelectedRow();
       // String message = docMsgTxtField.getText();

        if (selectedRow < 0){
            JOptionPane.showMessageDialog(null, "Please select Row !!");
           // String message = docMsgTxtField.getText();
        }
        else{
             Details request = (Details)wRJTbl.getValueAt(selectedRow, 0);
          // WorkRequest request = (WorkRequest)wRJTbl.getValueAt(selectedRow, 0);
           JOptionPane.showMessageDialog(null, "Congrats you are Admited to Market Oriented Skill Development !!");
        request.setSchool("Admited to MOS");
        //request.setStatus("Completed");
        Organization org = null;
        for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()){
            if (organization instanceof MarketOrientedSkillOragnization){
                org = organization;
                break;
            }
        }
        if (org!=null){
            org.getDetailsdirectory().createDeatilsDir(request.getUniqueId(),"MOS");
        }
        
        populateTable();
            }
    }//GEN-LAST:event_MOSBtnActionPerformed

    private void empBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_empBtnActionPerformed
        // TODO add your handling code here:
        int selectedRow = wRJTbl.getSelectedRow();
       // String message = docMsgTxtField.getText();

        if (selectedRow < 0){
            JOptionPane.showMessageDialog(null, "Please select Row !!");
           // String message = docMsgTxtField.getText();
        }
        else{
            Details request = (Details)wRJTbl.getValueAt(selectedRow, 0);
           //WorkRequest request = (WorkRequest)wRJTbl.getValueAt(selectedRow, 0);
           JOptionPane.showMessageDialog(null, "Application sent to employmentt office !!");
        request.setSchool("Sent to emplyoment office");
        //request.setStatus("Completed");
        Organization org = null;
          
         for(Enterprise ent: network.getEnterpriseDirectory().getEnterpriseList()){
            for (Organization organization : ent.getOrganizationDirectory().getOrganizationList()){
                if (organization instanceof EmploymentInterviewer){
                    org = organization;
                    break;
                }
            }
        }
        if (org!=null){
            org.getDetailsdirectory().createDeatilsDir(request.getUniqueId(),"Employment");
        }
        
        populateTable();
            }
    }//GEN-LAST:event_empBtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton MOSBtn;
    private javax.swing.JButton aHighSchoolBtn;
    private javax.swing.JButton admitToSchollBtn;
    private javax.swing.JButton empBtn;
    private javax.swing.JLabel interviewerBckImageLbl;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton refreshJButton;
    private javax.swing.JTable wRJTbl;
    // End of variables declaration//GEN-END:variables
}
